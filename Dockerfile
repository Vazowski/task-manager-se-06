FROM java:8
COPY ./target/task-manager-se-06.jar /opt/task-manager-se-06.jar
WORKDIR /opt

ENTRYPOINT ["java", "-jar", "task-manager-se-06.jar"]

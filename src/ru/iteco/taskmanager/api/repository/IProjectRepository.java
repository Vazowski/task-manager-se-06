package ru.iteco.taskmanager.api.repository;

import java.util.List;

import ru.iteco.taskmanager.entity.Project;

public interface IProjectRepository {

	void merge(String name, String description, String uuid, String userId);
	List<Project> findAll();
	Project find(String name);
	void remove(String name);
	void removeAll();
	
}

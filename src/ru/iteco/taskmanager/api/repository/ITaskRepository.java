package ru.iteco.taskmanager.api.repository;

import java.util.List;

import ru.iteco.taskmanager.entity.Task;

public interface ITaskRepository {
	
	void merge(String name, String description, String uuid, String projectUuid, String userId);
	List<Task> findAll(String projectUuid);
	Task findByName(String name);
	void remove(String name);
	void removeAll(String projectUuid);
}

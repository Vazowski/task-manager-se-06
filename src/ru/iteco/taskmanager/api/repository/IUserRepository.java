package ru.iteco.taskmanager.api.repository;

import java.util.List;

import ru.iteco.taskmanager.entity.RoleType;
import ru.iteco.taskmanager.entity.User;

public interface IUserRepository {

	void merge(String login, String passwordHash, RoleType roleType, String uuid);
	String getPasswordHash(String uuid);
	User getUser(String login);
	List<User> getUsers();
}

package ru.iteco.taskmanager.api.service;

import java.util.List;

import ru.iteco.taskmanager.entity.Project;

public interface IProjectService {

	void merge(String name, String description, String uuid, String userId);
	boolean isExist(String projectName);
	Project find(String name);
	List<Project> findAll();
	void remove(String name);
	void removeAll();
}

package ru.iteco.taskmanager.api.service;

import java.util.List;

import ru.iteco.taskmanager.entity.Task;

public interface ITaskService {

	void merge(String name, String description, String uuid, String projectUuid, String userId);
	Task find(String name);
	List<Task> findAll(String projectUuid);
	void remove(String name);
	void removeAll(String projectUuid);
	boolean isExist(String name);
}

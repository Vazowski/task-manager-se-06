package ru.iteco.taskmanager.api.service;

import java.util.List;

import ru.iteco.taskmanager.entity.User;

public interface IUserService {

	User findByName(String login);
	User findByPassword(String login, String passwordHash);
	List<User> findAll();
	void persist(String login, String passwordHash);
	void merge(String login, String passwordHash);
	void update(String oldLogin, String newLogin);
}

package ru.iteco.taskmanager.bootstrap;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;

import ru.iteco.taskmanager.api.service.locator.IServiceLocator;
import ru.iteco.taskmanager.command.AbstractCommand;
import ru.iteco.taskmanager.command.ExitCommand;
import ru.iteco.taskmanager.command.HelpCommand;
import ru.iteco.taskmanager.command.project.ProjectFindAllCommand;
import ru.iteco.taskmanager.command.project.ProjectFindCommand;
import ru.iteco.taskmanager.command.project.ProjectMergeCommand;
import ru.iteco.taskmanager.command.project.ProjectPersistCommand;
import ru.iteco.taskmanager.command.project.ProjectRemoveAllCommand;
import ru.iteco.taskmanager.command.project.ProjectRemoveCommand;
import ru.iteco.taskmanager.command.task.TaskFindAllCommand;
import ru.iteco.taskmanager.command.task.TaskFindCommand;
import ru.iteco.taskmanager.command.task.TaskMergeCommand;
import ru.iteco.taskmanager.command.task.TaskPersistCommand;
import ru.iteco.taskmanager.command.task.TaskRemoveAllCommand;
import ru.iteco.taskmanager.command.task.TaskRemoveCommand;
import ru.iteco.taskmanager.command.user.UserCreateCommand;
import ru.iteco.taskmanager.command.user.UserLoginCommand;
import ru.iteco.taskmanager.command.user.UserLogoutCommand;
import ru.iteco.taskmanager.command.user.UserShowInfoCommand;
import ru.iteco.taskmanager.command.user.UserUpdateInfoCommand;
import ru.iteco.taskmanager.command.user.UserUpdatePasswordCommand;
import ru.iteco.taskmanager.entity.RoleType;
import ru.iteco.taskmanager.repository.ProjectRepository;
import ru.iteco.taskmanager.repository.TaskRepository;
import ru.iteco.taskmanager.service.TerminalService;
import ru.iteco.taskmanager.service.ProjectService;
import ru.iteco.taskmanager.service.TaskService;
import ru.iteco.taskmanager.service.UserService;

public class Bootstrap implements IServiceLocator {

	private Scanner scanner = new Scanner(System.in);
	private String inputCommand;
	
	private static final String DEFAULT_PASSWORD = "1234";
	
	private TerminalService terminalService = new TerminalService();
	public ProjectService projectService = new ProjectService(new ProjectRepository(), new TaskRepository());
	public TaskService taskService = new TaskService(new TaskRepository());
	public UserService userService = new UserService();
	
	public void init() {
		createDefaultUsers();
    	System.out.println("Enter command (type help for more information)");
    	
    	terminalService.addCommand("help", new HelpCommand(this, scanner));
    	terminalService.addCommand("exit", new ExitCommand(this, scanner));
    	terminalService.addCommand("login", new UserLoginCommand(this, scanner));
    	terminalService.addCommand("logout", new UserLogoutCommand(this, scanner));
    	terminalService.addCommand("user-create", new UserCreateCommand(this, scanner));
    	terminalService.addCommand("user-new-password", new UserUpdatePasswordCommand(this, scanner));
    	terminalService.addCommand("user-show", new UserShowInfoCommand(this, scanner));
    	terminalService.addCommand("user-update", new UserUpdateInfoCommand(this, scanner));

    	terminalService.addCommand("project-merge", new ProjectMergeCommand(this, scanner));
    	terminalService.addCommand("project-persist", new ProjectPersistCommand(this, scanner));
    	terminalService.addCommand("project-find", new ProjectFindCommand(this, scanner));
    	terminalService.addCommand("project-find-all", new ProjectFindAllCommand(this, scanner));
    	terminalService.addCommand("project-remove", new ProjectRemoveCommand(this, scanner));
    	terminalService.addCommand("project-remove-all", new ProjectRemoveAllCommand(this, scanner));

    	terminalService.addCommand("task-merge", new TaskMergeCommand(this, scanner));
    	terminalService.addCommand("task-persist", new TaskPersistCommand(this, scanner));
    	terminalService.addCommand("task-find", new TaskFindCommand(this, scanner));
    	terminalService.addCommand("task-find-all", new TaskFindAllCommand(this, scanner));
    	terminalService.addCommand("task-remove", new TaskRemoveCommand(this, scanner));
    	terminalService.addCommand("task-remove-all", new TaskRemoveAllCommand(this, scanner));
    	
    	while(true) {
    		System.out.print("> ");    		
    		inputCommand = scanner.nextLine();
    		if (terminalService.getCommands().containsKey(inputCommand)) {
    			execute(inputCommand);
    		} else {
    			System.out.println("Command doesn't exist");
    		}
    	}
	}
	
	public void execute(String command) {
		if (!userService.getCurrentUser().equals("none") && command.equals("login")) {
			System.out.println("You already login. Type another command");
			return;
		}
		
		try {
			if (userService.getCurrentUser().equals("none")) {
				terminalService.getCommands().get("login").execute();
			} else {
				AbstractCommand tempCommand = terminalService.getCommands().get(command.toLowerCase());
				String currentRole = userService.findByName(userService.getCurrentUser()).getRoleType().toString();
				if (tempCommand.getRoles().contains(currentRole)) {
					tempCommand.execute();
				} else {
					System.out.println("Permission denied");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void createDefaultUsers() {
		userService.persist("root", userService.getPasswordHash(DEFAULT_PASSWORD), RoleType.ADMIN);
		userService.persist("guest", userService.getPasswordHash(DEFAULT_PASSWORD), RoleType.USER);
		execute("login");
	}

	@Override
	public ProjectService getProjectService() {
		return projectService;
	}

	@Override
	public TaskService getTaskService() {
		return taskService;
	}

	@Override
	public UserService getUserService() {
		return userService;
	}

	@Override
	public TerminalService getTerminalService() {
		return terminalService;
	}
}

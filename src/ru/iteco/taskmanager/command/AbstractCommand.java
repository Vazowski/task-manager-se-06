package ru.iteco.taskmanager.command;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import ru.iteco.taskmanager.api.service.locator.IServiceLocator;

public abstract class AbstractCommand {

	protected IServiceLocator serviceLocator;
	public Scanner scanner; 
	
	public AbstractCommand(IServiceLocator serviceLocator, Scanner scanner) {
		this.serviceLocator = serviceLocator;
		this.scanner = scanner;
	}
	
	public abstract String command();
	public abstract String description();
	public abstract void execute() throws Exception;
	
	public List<String> getRoles() {
		return new ArrayList<String>() { {
			add("User");
			add("Administrator");
		}
		};
	}
}

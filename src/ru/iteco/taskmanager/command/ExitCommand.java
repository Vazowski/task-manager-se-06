package ru.iteco.taskmanager.command;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import ru.iteco.taskmanager.api.service.locator.IServiceLocator;

public class ExitCommand extends AbstractCommand {

	public ExitCommand(IServiceLocator serviceLocator, Scanner scanner) {
		super(serviceLocator, scanner);
	}

	@Override
	public String command() {
		return "exit";
	}

	@Override
	public String description() {
		return "  -  exit the programm";
	}

	@Override
	public void execute() throws Exception {
		System.exit(0);
	}

	@Override
	public List<String> getRoles() {
		return new ArrayList<String>() { {
			add("User");
			add("Administrator");
		}
		};
	}
}

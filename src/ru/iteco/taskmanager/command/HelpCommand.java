package ru.iteco.taskmanager.command;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import ru.iteco.taskmanager.api.service.locator.IServiceLocator;

public class HelpCommand extends AbstractCommand {
	
	public HelpCommand(IServiceLocator serviceLocator, Scanner scanner) {
		super(serviceLocator, scanner);
	}

	@Override
	public String command() {
		return "help";
	}

	@Override
	public String description() {
		return "  -  show all commands";
	}
	
	@Override
	public void execute() throws Exception {
		System.out.println();
		for (String key : serviceLocator.getTerminalService().getCommands().keySet()) {
			if (!key.equals("login") && !key.equals("login")) {
				System.out.println(key + serviceLocator.getTerminalService().getCommands().get(key).description());
			}
		}
	}
	
	@Override
	public List<String> getRoles() {
		return new ArrayList<String>() { {
			add("User");
			add("Administrator");
		}
		};
	}
}

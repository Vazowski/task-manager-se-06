package ru.iteco.taskmanager.command.project;

import java.util.List;
import java.util.Scanner;

import ru.iteco.taskmanager.api.service.locator.IServiceLocator;
import ru.iteco.taskmanager.command.AbstractCommand;
import ru.iteco.taskmanager.entity.Project;

public class ProjectFindAllCommand extends AbstractCommand {

	public ProjectFindAllCommand(IServiceLocator serviceLocator, Scanner scanner) {
		super(serviceLocator, scanner);
	}

	@Override
	public String command() {
		return "project-find-all";
	}

	@Override
	public String description() {
		return "  -  find all project";
	}

	@Override
	public void execute() throws Exception {
		List<Project> tempList = serviceLocator.getProjectService().findAll();
		if (tempList.size() == 0) {
			System.out.println("Empty");
			return;
		}
		for (int i = 0; i < tempList.size(); i++) {
			System.out.println("[Project " + (i + 1) + "]");
			System.out.println(serviceLocator.getProjectService().find(tempList.get(i).getName()).toString());
		}
	}
}

package ru.iteco.taskmanager.command.project;

import java.util.Scanner;

import ru.iteco.taskmanager.api.service.locator.IServiceLocator;
import ru.iteco.taskmanager.command.AbstractCommand;

public class ProjectFindCommand extends AbstractCommand {
	
	private String inputName;
	
	public ProjectFindCommand(IServiceLocator serviceLocator, Scanner scanner) {
		super(serviceLocator, scanner);
	}

	@Override
	public String command() {
		return "project-find";
	}

	@Override
	public String description() {
		return "  -  find one project";
	}

	@Override
	public void execute() throws Exception {
		System.out.print("Name of project: ");
		inputName = scanner.nextLine();
		if (serviceLocator.getProjectService().find(inputName) != null) {
			System.out.println(serviceLocator.getProjectService().find(inputName).toString());
		} else {
			throw new Exception("No project with same name");
		}
	}
}

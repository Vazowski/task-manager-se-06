package ru.iteco.taskmanager.command.project;

import java.util.Scanner;
import java.util.UUID;

import ru.iteco.taskmanager.api.service.locator.IServiceLocator;
import ru.iteco.taskmanager.command.AbstractCommand;

public class ProjectMergeCommand extends AbstractCommand{

	private String inputName, inputDescription, uuid, findedUuid;

	public ProjectMergeCommand(IServiceLocator serviceLocator, Scanner scanner) {
		super(serviceLocator, scanner);
	}
	
	@Override
	public String command() {
		return "project-merge";
	}

	@Override
	public String description() {
		return "  -  merge project";
	}

	@Override
	public void execute() throws Exception {
		System.out.print("Name of project: ");
		inputName = scanner.nextLine();
		System.out.print("Description of project: ");
		inputDescription = scanner.nextLine();
		if (serviceLocator.getProjectService().isExist(inputName)) {
			findedUuid = serviceLocator.getProjectService().find(inputName).getUuid();
			serviceLocator.getProjectService().merge(inputName, inputDescription, findedUuid, serviceLocator.getUserService().findByName(serviceLocator.getUserService().getCurrentUser()).getUuid());
		} else {
			uuid = UUID.randomUUID().toString();
			serviceLocator.getProjectService().merge(inputName, inputDescription, uuid, serviceLocator.getUserService().findByName(serviceLocator.getUserService().getCurrentUser()).getUuid());
		}
		System.out.println("Done");
	}
}

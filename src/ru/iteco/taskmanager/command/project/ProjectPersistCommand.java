package ru.iteco.taskmanager.command.project;

import java.util.Scanner;
import java.util.UUID;

import ru.iteco.taskmanager.api.service.locator.IServiceLocator;
import ru.iteco.taskmanager.command.AbstractCommand;

public class ProjectPersistCommand extends AbstractCommand{

	private String inputName, inputDescription, uuid;

	public ProjectPersistCommand(IServiceLocator serviceLocator, Scanner scanner) {
		super(serviceLocator, scanner);
	}
	
	@Override
	public String command() {
		return "project-persist";
	}

	@Override
	public String description() {
		return "  -  persist project";
	}

	@Override
	public void execute() throws Exception {
		System.out.print("Name of project: ");
		inputName = scanner.nextLine();
		if (!serviceLocator.getProjectService().isExist(inputName)) {
			System.out.print("Description of project: ");
			inputDescription = scanner.nextLine();
			uuid = UUID.randomUUID().toString();
			serviceLocator.getProjectService().merge(inputName, inputDescription, uuid, serviceLocator.getUserService().findByName(serviceLocator.getUserService().getCurrentUser()).getUuid());
			System.out.println("Done");
		} else {
			throw new Exception("Project with same name already exist");
		}
	}
}

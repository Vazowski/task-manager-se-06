package ru.iteco.taskmanager.command.project;

import java.util.Scanner;

import ru.iteco.taskmanager.api.service.locator.IServiceLocator;
import ru.iteco.taskmanager.command.AbstractCommand;

public class ProjectRemoveAllCommand extends AbstractCommand{

	public ProjectRemoveAllCommand(IServiceLocator serviceLocator, Scanner scanner) {
		super(serviceLocator, scanner);
	}

	@Override
	public String command() {
		return "project-remove";
	}

	@Override
	public String description() {
		return "  -  remove one project";
	}

	@Override
	public void execute() throws Exception {
		serviceLocator.getProjectService().removeAll();
	}
}

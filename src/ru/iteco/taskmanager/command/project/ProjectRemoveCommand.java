package ru.iteco.taskmanager.command.project;

import java.util.Scanner;

import ru.iteco.taskmanager.api.service.locator.IServiceLocator;
import ru.iteco.taskmanager.command.AbstractCommand;

public class ProjectRemoveCommand extends AbstractCommand {

	private String inputName;
	
	public ProjectRemoveCommand(IServiceLocator serviceLocator, Scanner scanner) {
		super(serviceLocator, scanner);
	}
	
	@Override
	public String command() {
		return "project-remove";
	}

	@Override
	public String description() {
		return "  -  remove one project";
	}

	@Override
	public void execute() throws Exception {
		System.out.print("Name of project: ");
		inputName = scanner.nextLine();
		if (serviceLocator.getProjectService().find(inputName) != null) {
			serviceLocator.getProjectService().remove(inputName);
			System.out.println("Done");
		} else {
			System.out.println("Project doesn't exist");
		}
	}
}

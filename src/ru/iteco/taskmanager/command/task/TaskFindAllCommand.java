package ru.iteco.taskmanager.command.task;

import java.util.List;
import java.util.Scanner;

import ru.iteco.taskmanager.api.service.locator.IServiceLocator;
import ru.iteco.taskmanager.command.AbstractCommand;
import ru.iteco.taskmanager.entity.Task;

public class TaskFindAllCommand extends AbstractCommand {
	
	private String inputProjectName, projectUuid;
	
	public TaskFindAllCommand(IServiceLocator serviceLocator, Scanner scanner) {
		super(serviceLocator, scanner);
	}

	@Override
	public String command() {
		return "task-find-all";
	}

	@Override
	public String description() {
		return "  -  find all task in project";
	}

	@Override
	public void execute() throws Exception {
		System.out.print("Name of project: ");
		inputProjectName = scanner.nextLine();
		if (serviceLocator.getProjectService().find(inputProjectName) == null) {
			System.out.println("Project doesn't exist");
		} else {
			projectUuid = serviceLocator.getProjectService().find(inputProjectName).getUuid();
			List<Task> tempList = serviceLocator.getTaskService().findAll(projectUuid);
			if (tempList.size() == 0) {
				System.out.println("Empty");
				return;
			}
			for (int i = 0; i < tempList.size(); i++) {
				System.out.println("[Task " + (i + 1) + "]");
				System.out.println(serviceLocator.getTaskService().find(tempList.get(i).getName()).toString());
			}
		}
	}
}

package ru.iteco.taskmanager.command.task;

import java.util.Scanner;

import ru.iteco.taskmanager.api.service.locator.IServiceLocator;
import ru.iteco.taskmanager.command.AbstractCommand;

public class TaskFindCommand extends AbstractCommand {

	private String inputProjectName, inputName;
	
	public TaskFindCommand(IServiceLocator serviceLocator, Scanner scanner) {
		super(serviceLocator, scanner);
	}

	@Override
	public String command() {
		return "task-find";
	}

	@Override
	public String description() {
		return "  -  find one task in project";
	}

	@Override
	public void execute() throws Exception {
		System.out.print("Name of project: ");
		inputProjectName = scanner.nextLine();
		if (serviceLocator.getProjectService().find(inputProjectName) == null) {
			System.out.println("Project doesn't exist");
		} else {
			System.out.print("Name of task: ");
			inputName = scanner.nextLine();
			if (serviceLocator.getTaskService().find(inputName) != null) {
				System.out.println(serviceLocator.getTaskService().find(inputName).toString());
			} else {
				throw new Exception("No task with same name");
			}
		}
	}
}

package ru.iteco.taskmanager.command.task;

import java.util.Scanner;
import java.util.UUID;

import ru.iteco.taskmanager.api.service.locator.IServiceLocator;
import ru.iteco.taskmanager.command.AbstractCommand;

public class TaskMergeCommand extends AbstractCommand {

	private String inputProjectName, inputName, inputDescription, uuid, findedUuid, projectUuid;
	
	public TaskMergeCommand(IServiceLocator serviceLocator, Scanner scanner) {
		super(serviceLocator, scanner);
	}

	@Override
	public String command() {
		return "task-merge";
	}

	@Override
	public String description() {
		return "  -  merge task";
	}

	@Override
	public void execute() throws Exception {
		System.out.print("Name of project: ");
		inputProjectName = scanner.nextLine();
		if (serviceLocator.getProjectService().find(inputProjectName) == null) {
			System.out.println("Project doesn't exist");
		} else {
			projectUuid = serviceLocator.getProjectService().find(inputProjectName).getUuid();
			System.out.print("Name of task: ");
			inputName = scanner.nextLine();
			System.out.print("Description of task: ");
			inputDescription = scanner.nextLine();
			if (serviceLocator.getTaskService().isExist(inputName)) {
				findedUuid = serviceLocator.getTaskService().find(inputName).getUuid();
				serviceLocator.getTaskService().merge(inputName, inputDescription, findedUuid, projectUuid, serviceLocator.getUserService().findByName(serviceLocator.getUserService().getCurrentUser()).getUuid());
			} else {
				uuid = UUID.randomUUID().toString();
				serviceLocator.getTaskService().merge(inputName, inputDescription, uuid, projectUuid, serviceLocator.getUserService().findByName(serviceLocator.getUserService().getCurrentUser()).getUuid());
			}
			System.out.println("Done");
		}
	}
}

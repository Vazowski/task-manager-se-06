package ru.iteco.taskmanager.command.task;

import java.util.Scanner;
import java.util.UUID;

import ru.iteco.taskmanager.api.service.locator.IServiceLocator;
import ru.iteco.taskmanager.command.AbstractCommand;

public class TaskPersistCommand extends AbstractCommand {

	private String inputProjectName, inputName, inputDescription, uuid, projectUuid;
	
	public TaskPersistCommand(IServiceLocator serviceLocator, Scanner scanner) {
		super(serviceLocator, scanner);
	}

	@Override
	public String command() {
		return "task-persist";
	}

	@Override
	public String description() {
		return "  -  persist task";
	}

	@Override
	public void execute() throws Exception {
		System.out.print("Name of project: ");
		inputProjectName = scanner.nextLine();
		if (serviceLocator.getProjectService().find(inputProjectName) == null) {
			System.out.println("Project doesn't exist");
		} else {
			projectUuid = serviceLocator.getProjectService().find(inputProjectName).getUuid();
			System.out.print("Name of task: ");
			inputName = scanner.nextLine();
			if (serviceLocator.getTaskService().isExist(inputName)) {
				throw new Exception("Task with same name already exist");
			} else {
				System.out.print("Description of task: ");
				inputDescription = scanner.nextLine();
				uuid = UUID.randomUUID().toString();
				serviceLocator.getTaskService().merge(inputName, inputDescription, uuid, projectUuid, serviceLocator.getUserService().findByName(serviceLocator.getUserService().getCurrentUser()).getUuid());
				System.out.println("Done");
			}
		}
	}
}

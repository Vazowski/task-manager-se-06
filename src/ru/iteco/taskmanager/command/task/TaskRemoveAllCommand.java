package ru.iteco.taskmanager.command.task;

import java.util.Scanner;

import ru.iteco.taskmanager.api.service.locator.IServiceLocator;
import ru.iteco.taskmanager.command.AbstractCommand;

public class TaskRemoveAllCommand extends AbstractCommand {

	private String inputProjectName, projectUuid;
	
	public TaskRemoveAllCommand(IServiceLocator serviceLocator, Scanner scanner) {
		super(serviceLocator, scanner);
	}

	@Override
	public String command() {
		return "task-remove-all";
	}

	@Override
	public String description() {
		return "  -  remove all task in project";
	}

	@Override
	public void execute() throws Exception {
		System.out.print("Name of project: ");
		inputProjectName = scanner.nextLine();
		if (serviceLocator.getProjectService().find(inputProjectName) == null) {
			System.out.println("Project doesn't exist");
		} else {
			projectUuid = serviceLocator.getProjectService().find(inputProjectName).getUuid();
			serviceLocator.getTaskService().removeAll(projectUuid);
			System.out.println("Done");
		}
	}
}

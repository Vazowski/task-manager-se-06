package ru.iteco.taskmanager.command.task;

import java.util.Scanner;

import ru.iteco.taskmanager.api.service.locator.IServiceLocator;
import ru.iteco.taskmanager.command.AbstractCommand;

public class TaskRemoveCommand extends AbstractCommand {

	private String inputProjectName, inputName, findedUuid;
	
	public TaskRemoveCommand(IServiceLocator serviceLocator, Scanner scanner) {
		super(serviceLocator, scanner);
	}

	@Override
	public String command() {
		return "task-remove";
	}

	@Override
	public String description() {
		return "  -  remove task from project";
	}

	@Override
	public void execute() throws Exception {
		System.out.print("Name of project: ");
		inputProjectName = scanner.nextLine();
		if (serviceLocator.getProjectService().find(inputProjectName) == null) {
			System.out.println("Project doesn't exist");
		} else {
			System.out.print("Name of task: ");
			inputName = scanner.nextLine();
			if (serviceLocator.getTaskService().isExist(inputName)) {
				findedUuid = serviceLocator.getTaskService().find(inputName).getUuid();
				serviceLocator.getTaskService().remove(inputName);
				System.out.println("Done");
			} else {
				System.out.println("Task doesn't exist");
			}
		}
	}
}

package ru.iteco.taskmanager.command.user;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import ru.iteco.taskmanager.api.service.locator.IServiceLocator;
import ru.iteco.taskmanager.command.AbstractCommand;

public class UserCreateCommand extends AbstractCommand {
	
	public UserCreateCommand(IServiceLocator serviceLocator, Scanner scanner) {
		super(serviceLocator, scanner);
	}

	@Override
	public String command() {
		return "user-create";
	}

	@Override
	public String description() {
		return "  -  create new user";
	}

	@Override
	public void execute() throws Exception {
		while (true) {
			System.out.print("Username: ");
			String name = scanner.nextLine();
			if (serviceLocator.getUserService().findByName(name) != null) {
				System.out.println("Username exist");
			} else {
				System.out.print("Password: ");
				String password = scanner.nextLine();
				serviceLocator.getUserService().persist(name, serviceLocator.getUserService().getPasswordHash(password));
				System.out.println("Done");
				break;
			}
		}
	}

	@Override
	public List<String> getRoles() {
		return new ArrayList<String>() { {
			add("Administrator");
		}
		};
	}
}

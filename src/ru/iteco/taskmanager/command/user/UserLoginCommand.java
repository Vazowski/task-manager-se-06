package ru.iteco.taskmanager.command.user;

import java.util.Scanner;

import ru.iteco.taskmanager.api.service.locator.IServiceLocator;
import ru.iteco.taskmanager.command.AbstractCommand;

public class UserLoginCommand extends AbstractCommand {

	private String login, password;
	private String hashPass;
	
	public UserLoginCommand(IServiceLocator serviceLocator, Scanner scanner) {
		super(serviceLocator, scanner);
	}

	@Override
	public String command() {
		return "login";
	}

	@Override
	public String description() {
		return "  -  login user";
	}

	@Override
	public void execute() throws Exception {
		while(true) {
			System.out.print("Login: ");
			login = scanner.nextLine();
			System.out.print("Password: ");
			password = scanner.nextLine();
			hashPass = serviceLocator.getUserService().getPasswordHash(password);
			if (serviceLocator.getUserService().findByPassword(login, hashPass) != null) {
				System.out.println("Success");
				if (password.equals("0000")) {
					System.out.print("Please type your new password: ");
					password = scanner.nextLine();
					serviceLocator.getUserService().merge(login, serviceLocator.getUserService().getPasswordHash(password));
					System.out.println("Done");
				}
				serviceLocator.getUserService().setCurrentUser(login);
				break;
			} else {
				System.out.println("Username or password was incorrect. Try again");
			}
		}
	}
}

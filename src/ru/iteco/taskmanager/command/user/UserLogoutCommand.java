package ru.iteco.taskmanager.command.user;

import java.util.Scanner;

import ru.iteco.taskmanager.api.service.locator.IServiceLocator;
import ru.iteco.taskmanager.command.AbstractCommand;

public class UserLogoutCommand extends AbstractCommand{
	
	public UserLogoutCommand(IServiceLocator serviceLocator, Scanner scanner) {
		super(serviceLocator, scanner);
	}

	@Override
	public String command() {
		return "logout";
	}

	@Override
	public String description() {
		return "  -  user logout";
	}

	@Override
	public void execute() throws Exception {
		serviceLocator.getUserService().setCurrentUser("none");
		System.out.println("Done");
		serviceLocator.getTerminalService().getCommand("login").execute();
	}

}

package ru.iteco.taskmanager.command.user;

import java.util.Scanner;

import ru.iteco.taskmanager.api.service.locator.IServiceLocator;
import ru.iteco.taskmanager.command.AbstractCommand;

public class UserShowInfoCommand extends AbstractCommand {

	public UserShowInfoCommand(IServiceLocator serviceLocator, Scanner scanner) {
		super(serviceLocator, scanner);
	}

	@Override
	public String command() {
		return "user-show";
	}

	@Override
	public String description() {
		return "  -  show user info";
	}

	@Override
	public void execute() throws Exception {
		System.out.println("Current name: " + serviceLocator.getUserService().getCurrentUser());
		System.out.println("Current role: " + serviceLocator.getUserService().findByName(serviceLocator.getUserService().getCurrentUser()).getRoleType());
	}
}

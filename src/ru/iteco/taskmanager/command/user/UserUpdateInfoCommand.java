package ru.iteco.taskmanager.command.user;

import java.util.Scanner;

import ru.iteco.taskmanager.api.service.locator.IServiceLocator;
import ru.iteco.taskmanager.command.AbstractCommand;

public class UserUpdateInfoCommand extends AbstractCommand {

	public UserUpdateInfoCommand(IServiceLocator serviceLocator, Scanner scanner) {
		super(serviceLocator, scanner);
	}

	@Override
	public String command() {
		return "user-update";
	}

	@Override
	public String description() {
		return "  -  update user information";
	}

	@Override
	public void execute() throws Exception {
		System.out.print("New name: ");
		String name = scanner.nextLine();
		serviceLocator.getUserService().update(serviceLocator.getUserService().getCurrentUser(), name);
		serviceLocator.getUserService().setCurrentUser(name);
		System.out.println("Done");
	}
}

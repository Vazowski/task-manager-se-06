package ru.iteco.taskmanager.command.user;

import java.util.Scanner;

import ru.iteco.taskmanager.api.service.locator.IServiceLocator;
import ru.iteco.taskmanager.command.AbstractCommand;

public class UserUpdatePasswordCommand extends AbstractCommand {

	public UserUpdatePasswordCommand(IServiceLocator serviceLocator, Scanner scanner) {
		super(serviceLocator, scanner);
	}

	@Override
	public String command() {
		return "user-new-password";
	}

	@Override
	public String description() {
		return "  -  create new password for user";
	}

	@Override
	public void execute() throws Exception {
		while(true) {
			System.out.print("Current password: ");
			String password = scanner.nextLine();
			if (serviceLocator.getUserService().findByPassword(serviceLocator.getUserService().getCurrentUser(), serviceLocator.getUserService().getPasswordHash(password)) != null) {
				System.out.print("New password: ");
				password = scanner.nextLine();
				serviceLocator.getUserService().merge(serviceLocator.getUserService().getCurrentUser(), serviceLocator.getUserService().getPasswordHash(password));
				System.out.println("Done");
				break;
			} else {
				System.out.println("Entered password is incorrect");
			}
		}
	}
}

package ru.iteco.taskmanager.entity;

import java.text.SimpleDateFormat;

public class AbstractEntity {

	protected SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
	
	protected String uuid;
	
	public AbstractEntity() {
		
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
}

package ru.iteco.taskmanager.entity;

import java.util.Date;

public class Project extends AbstractEntity {

	private String name;
	private String description;
	private String dateBegin;
	private String dateEnd;
	private String ownerId;
	private String type;
	
	public Project() {
    	
    }
    
    public Project(String name, String description, String uuid, String ownerId) {
    	this.uuid = uuid;
    	this.name = name;
    	this.description = description;
    	this.ownerId = ownerId;
    	this.dateBegin = dateFormat.format(new Date());
    	this.dateEnd = dateFormat.format(new Date());
    	this.type = "Project";
    }
    
	@Override
	public String toString() {
		return "Name: " + this.name + "\nDescription: " + this.description + "\nDateBegin: " + this.dateBegin + "\nDateEnd: " + this.dateEnd + "\nUUID: " + this.uuid; 
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDateBegin() {
		return dateBegin;
	}

	public void setDateBegin(String dateBegin) {
		this.dateBegin = dateBegin;
	}

	public String getDateEnd() {
		return dateEnd;
	}

	public void setDateEnd(String dateEnd) {
		this.dateEnd = dateEnd;
	}

	public String getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}

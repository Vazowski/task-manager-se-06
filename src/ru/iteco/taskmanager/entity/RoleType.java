package ru.iteco.taskmanager.entity;

public enum RoleType {

	ADMIN ("Administrator"),
	USER ("User");
	
	private String displayName;

	RoleType(String displayName) {
		this.displayName = displayName;
	}

	public String getTitle() {
		return displayName;
	}

	@Override
	public String toString() {
		return displayName;
	}

}

package ru.iteco.taskmanager.entity;

public class User extends AbstractEntity {

	private String login;
	private String passwordHash;
	private RoleType roleType;
	
	public User(String login, String passwordHash, RoleType roleType, String uuid) {
		this.login = login;
		this.passwordHash = passwordHash;
		this.roleType = roleType;
		this.uuid = uuid;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash;
	}
	
	public String getPasswordHash() {
		return passwordHash;
	}

	public RoleType getRoleType() {
		return roleType;
	}

	public String getUuid() {
		return uuid;
	}
}

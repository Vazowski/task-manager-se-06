package ru.iteco.taskmanager.repository;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import ru.iteco.taskmanager.entity.Project;

public class ProjectRepository {

	private static Map<String, Project> projectMap  = new LinkedHashMap<String, Project>();
	
	public ProjectRepository() {
		
	}
	
	public void merge(String name, String description, String uuid, String userId) {
		projectMap.put(uuid, new Project(name, description, uuid, userId));
	}

	public List<Project> findAll() {
		List<Project> result = new ArrayList<Project>();
		for (Project project : projectMap.values()) {
			result.add(project);
		}
		return result;
	}

	public Project find(String name) {
		for (Project project : projectMap.values()) {
			if (project.getName().equals(name))
				return project;
		}
		return null;
	}

	public void remove(String name) {
		projectMap.remove(find(name).getUuid());
	}

	public void removeAll() {
		projectMap.clear();
	}
}

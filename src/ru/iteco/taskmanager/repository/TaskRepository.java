package ru.iteco.taskmanager.repository;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import ru.iteco.taskmanager.entity.Task;

public class TaskRepository {

	private Map<String, Task> taskMap = new LinkedHashMap<String, Task>();
	
	public TaskRepository() {

	}
	
	public void merge(String name, String description, String uuid, String projectUuid, String userId) {
		taskMap.put(uuid, new Task(name, description, uuid, projectUuid, userId));
	}
	
	public List<Task> findAll(String projectUuid) {
		List<Task> result = new ArrayList<>();
		for (Task task : taskMap.values()) {
			if (projectUuid.equals(task.getProjectUUID())) {
				result.add(getByName(task.getName()));
			}
		}
		return result;
	}
	
	public Task getByName(String name) {
		for (Task task : taskMap.values()) {
			if (task.getName().equals(name))
				return task;
		}
		return null;
	}

	public void remove(String name) {
		taskMap.remove(getByName(name).getUuid());
	}
	
	public void removeAll(String projectUuid) {
		Iterator<Task> iterator = taskMap.values().iterator();
	    
	    while(iterator.hasNext()){
	      Task task = iterator.next();
	      if (projectUuid.equals(task.getProjectUUID())) {
	    	  iterator.remove();
	      }
	    }
	}
}

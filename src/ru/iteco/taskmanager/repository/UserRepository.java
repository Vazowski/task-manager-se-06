package ru.iteco.taskmanager.repository;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import ru.iteco.taskmanager.api.repository.IUserRepository;
import ru.iteco.taskmanager.entity.RoleType;
import ru.iteco.taskmanager.entity.User;

public class UserRepository implements IUserRepository {

	private static Map<String, User> userMap = new LinkedHashMap<String, User>();
	
	public UserRepository() {

	}
	
	public void merge(String login, String passwordHash, RoleType roleType, String uuid) {
		userMap.put(uuid, new User(login, passwordHash, roleType, uuid));
	}
	
	public String getPasswordHash(String uuid) {
		return userMap.get(uuid).getPasswordHash();
	}
	
	public User getUser(String login) {
		for (User user : userMap.values()) {
			if (user.getLogin().equals(login))
				return user;
		}
		return null;
	}
	
	public List<User> getUsers() {
		List<User> result = new ArrayList<User>();
		for (User user : userMap.values()) {
			result.add(user);
		}
		return result;
	}
}

package ru.iteco.taskmanager.service;

import java.util.List;

import ru.iteco.taskmanager.entity.Project;
import ru.iteco.taskmanager.repository.ProjectRepository;
import ru.iteco.taskmanager.repository.TaskRepository;

public class ProjectService  {
	
	private ProjectRepository projectRepository;
	private TaskRepository taskRepository;
	
	public ProjectService(ProjectRepository projectRepository, TaskRepository taskRepository) {
		this.projectRepository = projectRepository;
		this.taskRepository = taskRepository;
	}
	
	public void merge(String name, String description, String uuid, String userId) {
		projectRepository.merge(name, description, uuid, userId);
	}
	
	public boolean isExist(String projectName) {
		return !(projectRepository.find(projectName) == null);
	}
	
	public Project find(String name) {
		return projectRepository.find(name);
	}
	
	public List<Project> findAll() {
		return projectRepository.findAll();
	}
	
	public void remove(String name) {
		taskRepository.removeAll(name);
		projectRepository.remove(name);
	}
	
	public void removeAll() {
		List<Project> tempList = findAll();
		for (int i = 0; i < tempList.size(); i++) {
			taskRepository.removeAll(tempList.get(i).getUuid());
		}
		projectRepository.removeAll();
		System.out.println("Done");
	}
	
	
}

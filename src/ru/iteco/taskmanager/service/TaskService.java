package ru.iteco.taskmanager.service;

import java.util.List;

import ru.iteco.taskmanager.entity.Task;
import ru.iteco.taskmanager.repository.TaskRepository;

public class TaskService {

	private TaskRepository taskRepository; 
	
	public TaskService(TaskRepository taskRepository) {
		this.taskRepository = taskRepository;
	}
	
	public void merge(String name, String description, String uuid, String projectUuid, String userId) {
		taskRepository.merge(name, description, uuid, projectUuid, userId);
	}
		
	public Task find(String name) {
		return taskRepository.getByName(name);
	}
	
	public List<Task> findAll(String projectUuid) {
		return taskRepository.findAll(projectUuid);
	}
	
	public void remove(String name) {
		taskRepository.remove(name);
	}
	
	public void removeAll(String projectUuid) {
		taskRepository.removeAll(projectUuid);
	}
	
	public boolean isExist(String name) {
		return !(find(name) == null);
	}
}

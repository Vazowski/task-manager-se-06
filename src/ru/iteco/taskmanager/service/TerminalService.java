package ru.iteco.taskmanager.service;

import java.util.Map;

import ru.iteco.taskmanager.command.AbstractCommand;

public class TerminalService {

	private Map<String, AbstractCommand> commandsMap;
	
	public TerminalService() {
		
	}
	
	public void addCommand(String command, AbstractCommand abstractCommand) {
		commandsMap.put(command, abstractCommand);
	}
	
	public AbstractCommand getCommand(String command) {
		return commandsMap.get(command);
	}
	
	public Map<String, AbstractCommand> getCommands() {
		return commandsMap;
	}
}

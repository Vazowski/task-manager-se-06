package ru.iteco.taskmanager.service;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.UUID;

import ru.iteco.taskmanager.entity.RoleType;
import ru.iteco.taskmanager.entity.User;
import ru.iteco.taskmanager.repository.UserRepository;

public class UserService {

	private UserRepository userRepository;
	private StringBuilder currentUser = new StringBuilder("none");
	
	public UserService() {
		userRepository = new UserRepository();
	}
	
	public User findByName(String login) {
		List<User> users = userRepository.getUsers();
		for (User user : users) {
			if (login.equals(user.getLogin()))
				return user;
		}
		return null;
	}
	
	public User findByPassword(String login, String passwordHash) {
		List<User> users = userRepository.getUsers();
		for (User user : users) {
			if (login.equals(user.getLogin()) && passwordHash.equals(user.getPasswordHash()))
				return user;
		}
		return null;
	}
	
	public List<User> findAll() {
		return userRepository.getUsers();
	}
	
	public void persist(String login, String passwordHash) {
		String uuid = UUID.randomUUID().toString();
		userRepository.merge(login, passwordHash, RoleType.USER, uuid);
	}
	
	public void persist(String login, String passwordHash, RoleType roleType) {
		String uuid = UUID.randomUUID().toString();
		userRepository.merge(login, passwordHash, roleType, uuid);
	}
	
	public void merge(String login, String passwordHash) {
		userRepository.merge(login, passwordHash, userRepository.getUser(login).getRoleType(), userRepository.getUser(login).getUuid());
	}
	
	public void update(String oldLogin, String newLogin) {
		userRepository.merge(newLogin, userRepository.getUser(oldLogin).getPasswordHash(), userRepository.getUser(oldLogin).getRoleType(), userRepository.getUser(oldLogin).getUuid());
	}
	
	public String getCurrentUser() {
		return currentUser.toString();
	}
	
	public void setCurrentUser(String login) {
		this.currentUser = new StringBuilder(login);
	}
	
	public String getPasswordHash(String password) {
		StringBuffer sb = new StringBuffer();
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(password.getBytes());
			byte byteData[] = md.digest();
			for (int i = 0; i < byteData.length; i++)
				sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
			return sb.toString();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
}
